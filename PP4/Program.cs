﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP4
{
    class Program
    {

       
        static void Main(string[] args)
        {
            int n;
            Console.WriteLine("Introduceti n: ");
            n = int.Parse(Console.ReadLine());
            double[] v = new double[n];
            Console.WriteLine("Introduceti valorile vectorului:");
            for (int i = 0; i < n; i++)
                v[i] = double.Parse(Console.ReadLine());
            double suma = 0;
            for(int i = 0; i < v.Length - 1; i+=2)
            {
                suma = v[i] + v[i + 1];
                Array.Resize(ref v, v.Length + 1);
                for (int j = v.Length - 1; j > i + 1; j--)
                    v[j] = v[j - 1];
                v[i + 1] = suma;
                suma = 0;
            }
            Console.WriteLine("Noul vector:");
            foreach (double nr in v)
                Console.WriteLine(nr);
            //test

            Console.ReadKey();

        }

    }
}
